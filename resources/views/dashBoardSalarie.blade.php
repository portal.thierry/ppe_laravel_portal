@include('importHeader')

<body id="dashBody">
<br>
<button class="btn btn-info" id="btnLogoutDash">
    <a id="btnLogout" onclick="$('#logout-form').submit();">
        Déconnexion
    </a>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
</button>
<br>
<br>
<div id="dashTitle">Bienvenue {{$salarie->prenom}}</div>
@if (Auth::user()->getRoleNames()[0] === "User")
<div class="flexContainer">
    <div class="card border-info mb-3" id="dashBox" onclick="location='/salarie/{{urlencode($salarie->num)}}'">
        <div class="card-header bg-transparent border-info"><br></div>
        <div class="card-body text-info">
            <br>
            <h5 class="card-title">Informations personelles</h5>
            <br>
            <p class="card-text">Consultez vos informations personnelles</p>
        </div>
        <div class="card-footer bg-transparent border-info"><br></div>
    </div>
    <div class="card border-info mb-3" id="dashBox" onclick="location='/dhs/add'">
        <div class="card-header bg-transparent border-info"><br></div>
        <div class="card-body text-info">
            <br>
            <h5 class="card-title">Demande d'heures supplémentaires</h5>
            <br>
            <p class="card-text">Faites des demandes d'heures supplémentaires auprès de la RH</p>
        </div>
        <div class="card-footer bg-transparent border-info"><br></div>
    </div>
    <div class="card border-info mb-3" id="dashBox" onclick="location='/dhs/salarie/{{$salarie->num}}'">
        <div class="card-header bg-transparent border-info"><br></div>
        <div class="card-body text-info">
            <br>
            <h5 class="card-title">Consulter les demandes d'heures supplémentaires</h5>
            <br>
            <p class="card-text">Consultez les demandes d'heures supplémentaires que vous avez effectué</p>
        </div>
        <div class="card-footer bg-transparent border-info"><br></div>
    </div>
</div>
@endif

@if (Auth::user()->getRoleNames()[0] === "Admin")
<div class="flexContainer">
    <div class="card border-info mb-3" id="dashBox" onclick="location='/salarie/{{$salarie->num}}'">
        <div class="card-header bg-transparent border-info"><br></div>
        <div class="card-body text-info">
            <br>
            <h5 class="card-title">Informations personelles</h5>
            <br>
            <p class="card-text">Consultez vos informations personnelles</p>
        </div>
        <div class="card-footer bg-transparent border-info"><br></div>
    </div>
    <div class="card border-info mb-3" id="dashBox" onclick="location='/users'">
        <div class="card-header bg-transparent border-info"><br></div>
        <div class="card-body text-info">
            <br>
            <h5 class="card-title">Gestion des comptes</h5>
            <br>
            <p class="card-text">Gérez les comptes des utilisateurs de GSB</p>
        </div>
        <div class="card-footer bg-transparent border-info"><br></div>
    </div>
    <div class="card border-info mb-3" id="dashBox" onclick="location='/salaries/supp'">
        <div class="card-header bg-transparent border-info"><br></div>
        <div class="card-body text-info">
            <br>
            <h5 class="card-title">Suppression des salariés</h5>
            <br>
            <p class="card-text">Supprimez définitivement les salariés dont la suppression a été demandée par la RH</p>
        </div>
        <div class="card-footer bg-transparent border-info"><br></div>
    </div>
</div>
@endif

@if (Auth::user()->getRoleNames()[0] === "RH")
<div class="flexContainer">
    <div class="card border-info mb-3" id="dashBox" onclick="location='/salarie/{{$salarie->num}}'">
        <div class="card-header bg-transparent border-info"><br></div>
        <div class="card-body text-info">
            <br>
            <h5 class="card-title">Informations personelles</h5>
            <br>
            <p class="card-text">Consultez vos informations personnelles</p>
        </div>
        <div class="card-footer bg-transparent border-info"><br></div>
    </div>
    <div class="card border-info mb-3" id="dashBox" onclick="location='/salaries'">
        <div class="card-header bg-transparent border-info"><br></div>
        <div class="card-body text-info">
            <br>
            <h5 class="card-title">Gestion des salariés</h5>
            <br>
            <p class="card-text">Gérez les salariés de GSB</p>
        </div>
        <div class="card-footer bg-transparent border-info"><br></div>
    </div>
    <div class="card border-info mb-3" id="dashBox" onclick="location='/dhs'">
        <div class="card-header bg-transparent border-info"><br></div>
        <div class="card-body text-info">
            <br>
            <h5 class="card-title">Gestion des heures supplémentaires</h5>
            <br>
            <p class="card-text">Toutes les demandes : Gérez les demandes d'heures supplémentaires effectuées par les salariés</p>
        </div>
        <div class="card-footer bg-transparent border-info"><br></div>
    </div>
    <div class="card border-info mb-3" id="dashBox" onclick="location='/dhs/nonValidee'">
        <div class="card-header bg-transparent border-info"><br></div>
        <div class="card-body text-info">
            <br>
            <h5 class="card-title">Gestion des heures supplémentaires</h5>
            <br>
            <p class="card-text">Demandes à valider : Gérez les demandes d'heures supplémentaires à valider effectuées par les salariés</p>
        </div>
        <div class="card-footer bg-transparent border-info"><br></div>
    </div>
</div>
@endif

</body>
{{--consulter info--}}
{{--consulter heures supp et faire demandes--}}

<?php
