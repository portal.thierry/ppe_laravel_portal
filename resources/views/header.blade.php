@include('importHeader')

<nav class="navbar navbar-expand-lg" id="navbar">
    <div class="container-fluid">
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>

            <a class="navbar-brand" href="/salaries">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-lines-fill" viewBox="0 0 16 16">
            <path d="M6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-5 6s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H1zM11 3.5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1h-4a.5.5 0 0 1-.5-.5zm.5 2.5a.5.5 0 0 0 0 1h4a.5.5 0 0 0 0-1h-4zm2 3a.5.5 0 0 0 0 1h2a.5.5 0 0 0 0-1h-2zm0 3a.5.5 0 0 0 0 1h2a.5.5 0 0 0 0-1h-2z"/>
            </svg>RH
            </a>


    <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
        <a class="nav-link active" aria-current="page" href="../redirect">Page d'accueuil</a>
        </li>
    </ul>

    <ul class="navbar-nav me-auto mb-2 mb-lg-1" id="navMenuDeroulant">
        <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="/dhs">Heures supplémentaires</a>
                <ul>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="/dhs">Toutes les demandes</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="/dhs/nonValidee">Demandes non-validées</a>
                    </li>
                </ul>
        </li>

    </ul>
    <ul class="navbar-nav me-auto mb-2 mb-lg-1">
        <li class="nav-item">
        <a class="nav-link active" aria-current="page" href="/salaries" id="navSalarie">Salariés</a>
        </li>
    </ul>
    <form class="d-flex" role="search" action="/dhs/findFromNav" method="GET">
        <input class="form-control me-2" type="search" name="id" placeholder="Rechercher une demande" aria-label="Search">
        <button class="btn btn-outline-success" type="submit">Rechercher</button>
    </form>

    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
            {{Auth::user()["name"]}}
        </li>
    </ul>

    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li>
            <a id="btnLogout" class="nav-link active" onclick="$('#logout-form').submit();">
                Déconnexion
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>
    </ul>
</div>
</div>
</nav>
