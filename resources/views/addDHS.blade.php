@include('importHeader')

<body>

<br>
<div id="updateTitle">
<h2>Ajout</h2>
</div>
<br>

<a href="javascript:history.go(-1)"><button type="submit" class="btn btn-secondary" id="retourUpdate">Retour</button></a>

<form method="POST" action="" id="formUpdate">

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @method('PUT')
    @csrf
    <div class="mb-3">
        <label class="form-label">NumSalarie</label>
        <input class="form-control" value={{Auth::user()['id']}} name="numSalarie" disabled>
    </div>
    <div class="mb-3">
        <label class="form-label">Description</label>
        <input class="form-control" value="" name="description">
    </div>
    <div class="mb-3">
        <label class="form-label">Date</label>
        <input placeholder="aaaa-mm-jj" class="form-control" value="" name="date">
    </div>
    <div class="mb-3">
        <label class="form-label">NbHeure</label>
        <input class="form-control" value="" name="nbHeure">
    </div>
    <div class="md-3">
        <label for="inputState">TarifHeure</label>
        <select id="inputState" class="form-control" name="tarifHeure">
            <option disabled selected>Sélectionnez un type d'heures supplémentaires</option>
            <option>1</option>
            <option>2</option>
            <option>3</option>
        </select>
    </div>
        <br>

    <button type="submit" class="btn btn-primary" id="submitAdd">Ajouter</button>
</form>
<br>
<br>
<br>
<br>
<br>
</body>
