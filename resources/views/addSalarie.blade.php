@include('importHeader')

<body>

<br>
<div id="updateTitle">
<h2>Ajout</h2>
</div>
<br>

<a href="javascript:history.go(-1)"><button type="submit" class="btn btn-secondary" id="retourUpdate">Retour</button></a>

<form method="POST" action="" id="formUpdate">

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @method('PUT')
    @csrf
    <div class="mb-3">
        <label class="form-label">Numéro</label>
        <input class="form-control" value="{{$salarie->num+1}}" name="num" disabled>
    </div>
    <div class="mb-3">
        <label class="form-label">Nom</label>
        <input class="form-control" value="" name="nom">
    </div>
    <div class="mb-3">
        <label class="form-label">Prénom</label>
        <input class="form-control" value="" name="prenom">
    </div>
    <div class="mb-3">
        <label class="form-label">Adresse</label>
        <input class="form-control" value="" name="adresse">
    </div>
    <div class="mb-3">
        <label class="form-label">Ville</label>
        <input class="form-control" value="" name="ville">
    </div>
    <div class="mb-3">
        <label class="form-label">Téléphone</label>
        <input class="form-control" value="" name="telephone">
    </div>
    <div class="mb-3">
        <label class="form-label">Mail</label>
        <input class="form-control" value="" name="mail">
    </div>
    <div class="md-3">
        <label for="inputState">Poste</label>
        <select id="inputState" class="form-control" name="poste">
            <option disabled selected>Sélectionnez un poste</option>
            <option>CEO</option>
            <option>Chercheur</option>
            <option>Commercial</option>
            <option>Comptable</option>
            <option>DSI</option>
            <option>Informaticien</option>
            <option>Juriste</option>
            <option>Pharmacien</option>
            <option>RH</option>
            <option>Redaction</option>
            <option>Restaurateur</option>
            <option>Secretaire</option>
            <option>Securite</option>
            <option>Technicien de surface</option>
        </select>
    </div>
        <br>
    <div class="mb-3">
        <label class="form-label">Salaire Brut</label>
        <input class="form-control" value="" name="salaireBrut">
    </div>

    <button type="submit" class="btn btn-primary" id="submitAdd">Ajouter</button>
</form>
<br>
<br>
<br>
<br>
<br>
</body>
