<!DOCTYPE html>
<html lang="fr">

@include('headerSalarie')

<body>
<br>

    @if (count($data) != 0)
        <table class="table table-striped table-hover table-bordered" id="tableau">
            <thead>
            <tr>
                <th scope="col" id="header1">Numéro</th>
                <th scope="col" id="header2">Nom</th>
                <th scope="col" id="header3">Prénom</th>
                <th scope="col" id="header4">Adresse</th>
                <th scope="col" id="header5">Ville</th>
                <th scope="col" id="header6">Téléphone</th>
                <th scope="col" id="header7">Mail</th>
                <th scope="col" id="header8">Poste</th>
                <th scope="col" id="header9">Salaire Brut</th>
                <th scope="col"></th>

            </tr>
            </thead>

            <tbody>
            @foreach($data as $salarie)

                @if ($salarie->suppression == '0')
                    <tr class="table-info">
                        <td>{{ $salarie->num }}</td>
                        <td>{{ $salarie->nom }}</td>
                        <td>{{ $salarie->prenom }}</td>
                        <td>{{ $salarie->adresse }}</td>
                        <td>{{ $salarie->ville }}</td>
                        <td>{{ $salarie->telephone }}</td>
                        <td>{{ $salarie->mail }}</td>
                        <td>{{ $salarie->poste }}</td>
                        <td>{{ $salarie->salaireBrut }}€</td>
                        <td><a href='/salarie/update/{{$salarie->num}}'><button type="button" class="btn btn-primary">Modifier</button></a></td>
                    </tr>
                @endif


                @if ($salarie->suppression == '1')
                    <tr class="table-warning">
                        <td>{{ $salarie->num }}</td>
                        <td>{{ $salarie->nom }}</td>
                        <td>{{ $salarie->prenom }}</td>
                        <td>{{ $salarie->adresse }}</td>
                        <td>{{ $salarie->ville }}</td>
                        <td>{{ $salarie->telephone }}</td>
                        <td>{{ $salarie->mail }}</td>
                        <td>{{ $salarie->poste }}</td>
                        <td>{{ $salarie->salaireBrut }}€</td>
                        <td><a href='/salarie/update/{{$salarie->num}}'><button type="button" class="btn btn-primary">Modifier</button></a></td>
                    </tr>
                @endif

            @endforeach
            </tbody>
        </table>
    @endif

    @if (count($data) == 0)
        <h3 class="text-center">Aucun élément n'a été trouvé</h3>
    @endif

    <a href='/salarie/add/'><button type="button" class="btn btn-success" id="btnAdd">Ajouter</button></a>

    <footer>
        @if (isset($data))
            <ul class="pagination justify-content-center mb-4">
                {{$data->links("pagination::bootstrap-4")}}
            </ul>
        @endif
    </footer>


</body>
</html>
