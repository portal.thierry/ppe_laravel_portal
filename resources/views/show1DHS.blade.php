<!DOCTYPE html>
<html lang="fr">

@include('headerSansRecherche')

<body>
<br>

    @if(isset($data[0]))

        <table class="table table-info table-striped table-hover table-bordered" id="tableau">
            <thead>
                <tr>

                    <th scope="col" id="header1">Id</th>
                    <th scope="col" id="header2">NumSalarie</th>
                    <th scope="col" id="header3" style="width: 50px">Description</th>
                    <th scope="col" id="header4">Date</th>
                    <th scope="col" id="header5">NbHeure</th>
                    <th scope="col" id="header6">Validée</th>
                    <th scope="col" id="header7">TarifHeure</th>
                    <th scope="col" id="header8">Modifier</th>
                    @if (Auth::user()->getRoleNames()[0] === "RH")
                        <th scope="col" id="header8" style="width: 50px">Etat de la demande</th>
                    @endif
                </tr>
            </thead>

            <tbody>
            @foreach($data as $dhs)
            @if ($dhs->validee == 0)
            <tr class="table-info">
                <td>{{ $dhs->id }}</td>
                <td>{{ $dhs->numSalarie }}</td>
                <td>{{ substr($dhs->description,0,50) }}</td>
                <td>{{ $dhs->date }}</td>
                <td>{{ $dhs->nbHeure }}</td>
                <td>{{ $dhs->validee }}</td>
                <td>{{ $dhs->tarifHeure }}</td>
                <td>
                    <a href='/dhs/update/{{$dhs->id}}'><button type="button" class="btn btn-primary">Modifier</button></a>
                </td>
                @if (Auth::user()->getRoleNames()[0] === "RH")
                    <td>
                        @if ($dhs->validee == '0')
                            <a href='/dhs/updateValidation/{{$dhs->id}}'><button type="button" class="btn btn-success">Valider</button></a>
                        @endif
                        @if ($dhs->validee == '0')
                            <a href='/dhs/updateRefus/{{$dhs->id}}'><button type="button" class="btn btn-danger">Refuser</button></a>
                        @endif
                    </td>
                @endif
            </tr>
        @endif

        @if ($dhs->validee == 1)
            <tr class="table-success">
                <td>{{ $dhs->id }}</td>
                <td>{{ $dhs->numSalarie }}</td>
                <td>{{ substr($dhs->description,0,50) }}</td>
                <td>{{ $dhs->date }}</td>
                <td>{{ $dhs->nbHeure }}</td>
                <td>{{ $dhs->validee }}</td>
                <td>{{ $dhs->tarifHeure }}</td>
                <td>
                    <a href='/dhs/update/{{$dhs->id}}'><button type="button" class="btn btn-primary">Modifier</button></a>
                </td>
                <td>
                    @if ($dhs->validee == '0')
                        <a href='/dhs/updateValidation/{{$dhs->id}}'><button type="button" class="btn btn-success">Valider</button></a>
                    @endif
                    @if ($dhs->validee == '0')
                        <a href='/dhs/updateRefus/{{$dhs->id}}'><button type="button" class="btn btn-danger">Refuser</button></a>
                    @endif
                </td>
            </tr>
            @endif

            @if ($dhs->validee == -1)
                <tr class="table-danger">
                    <td>{{ $dhs->id }}</td>
                    <td>{{ $dhs->numSalarie }}</td>
                    <td>{{ substr($dhs->description,0,50) }}</td>
                    <td>{{ $dhs->date }}</td>
                    <td>{{ $dhs->nbHeure }}</td>
                    <td>{{ $dhs->validee }}</td>
                    <td>{{ $dhs->tarifHeure }}</td>
                    <td>
                        <a href='/dhs/update/{{$dhs->id}}'><button type="button" class="btn btn-primary">Modifier</button></a>
                    </td>
                    <td>
                        @if ($dhs->validee == '0')
                            <a href='/dhs/updateValidation/{{$dhs->id}}'><button type="button" class="btn btn-success">Valider</button></a>
                        @endif
                        @if ($dhs->validee == '0')
                            <a href='/dhs/updateRefus/{{$dhs->id}}'><button type="button" class="btn btn-danger">Refuser</button></a>
                        @endif
                    </td>
                </tr>
            @endif
            @endforeach
            </tbody>
        </table>

    @endif

    @if(!isset($data[0]))
        <h3 class="text-center">Aucun salarié n'a été trouvé</h3>
    @endif

</body>
</html>
