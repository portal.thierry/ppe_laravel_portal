@extends('layouts.app')
@include('importHeader')
<body id="bgBody">
    <div class="pull-right" id="btnRetourShow">
        <a class="btn btn-secondary" href="{{ route('users.index') }}">Retour</a>
    </div>
@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2 class="text-info">Modifier l'utilisateur</h2>
            <br>
    </div>
</div>
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Oups!</strong> Il y a un problème avec les informations renseignées.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
{!! Form::model($user, ['method' => 'PATCH','route' => ['users.update', $user->id]]) !!}
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group text-info">
            <strong>Nom :</strong>
            {!! Form::text('name', null, array('placeholder' => 'Nom','class' => 'form-control')) !!}
            <br>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group text-info">
            <strong>Email :</strong>
            {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
            <br>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group text-info">
            <strong>Mot de passe</strong>
            {!! Form::password('password', array('placeholder' => 'Mot de passe','class' => 'form-control')) !!}
            <br>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group text-info">
            <strong>Confirmer le mot de passe :</strong>
            {!! Form::password('confirm-password', array('placeholder' => 'Confirmer le mot de passe','class' => 'form-control')) !!}
            <br>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group text-info">
            <strong>Role :</strong>
            {!! Form::select('roles[]', $roles,$userRole, array('class' => 'form-control','multiple')) !!}
            <br>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <br>
        <br>
        <button type="submit" class="btn btn-primary">Confirmer</button>
    </div>
</div>
{!! Form::close() !!}
@endsection
</body>
