@extends('layouts.app')
@include('importHeader')
<body id="bgBody">
    <div class="pull-right" id="btnRetourShow">
        <a class="btn btn-secondary" href="{{ route('users.index') }}">Retour</a>
    </div>
@section('content')
<br>
<br>
<br>
<br>
<br>
<div class="container" id="boxShow">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card border-info mb-3">


                <div class="card-body text-info">
                    <div class="card-header card-title bg-transparent border-info text-info" id="loginTitle"><h2>Utilisateur</h2></div>
                        <br>
                        <br>
                        <h5>Nom : {{ $user->name }}</h5>
                        <br>
                        <h5>Email : {{ $user->email }}</h5>
                        <br>
                        <h5>Role :
                            @if(!empty($user->getRoleNames()))
                                @foreach($user->getRoleNames() as $v)
                                    <span class="badge rounded-pill bg-dark">{{ $v }}</span>
                                @endforeach
                            @endif
                        </h5>

                        <br>
                    <br>
                    <div class="card-footer bg-transparent border-info"><br></div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
</body>
