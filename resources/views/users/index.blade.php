@extends('layouts.app')
@include('importHeader')
<body id="bgBody">
    <div class="pull-right" id="btnRetourShow">
        <a class="btn btn-secondary" href="redirect">Retour</a>
    </div>
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2 class="text-info">Gestion des utilisateurs</h2>
                <br>
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif
    <table class="table table-striped table-hover table-bordered" id="tableau">
        <thead>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Email</th>
                <th>Roles</th>
                <th>Action</th>
            </tr>
        </thead>
    @foreach ($data as $key => $user)
        <tr class="table-info">
            <td>{{ ++$i }}</td>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>
                @if(!empty($user->getRoleNames()))
                    @foreach($user->getRoleNames() as $v)
                        <span class="badge rounded-pill bg-dark">{{ $v }}</span>
                    @endforeach
                @endif
            </td>
            <td>
                <a class="btn btn-info" href="{{ route('users.show',$user->id) }}">Show</a>
                <a class="btn btn-primary" href="{{ route('users.edit',$user->id) }}">Edit</a>
            </td>
        </tr>
    @endforeach
    </table>
    <br>
    <br>
</body>
<footer>
    @if (isset($data))
        <ul class="pagination justify-content-center mb-4">
            {{$data->links("pagination::bootstrap-4")}}
        </ul>
    @endif
</footer>
@endsection
