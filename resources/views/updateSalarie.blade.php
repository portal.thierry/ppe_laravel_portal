@include('importHeader')

<body>

<br>
<div id="updateTitle">
<h2>Modification</h2>
</div>
<br>

<a href="javascript:history.go(-1)"><button type="submit" class="btn btn-secondary" id="retourUpdate">Retour</button></a>

<form method="POST" action="" id="formUpdate">

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @method('PUT')
    @csrf
    <div class="mb-3">
        <label class="form-label">Numéro</label>
        <input class="form-control" value="{{$salarie->num}}" name="num" disabled>
    </div>
    <div class="mb-3">
        <label class="form-label">Nom</label>
        <input class="form-control" value="{{$salarie->nom}}" name="nom">
    </div>
    <div class="mb-3">
        <label class="form-label">Prénom</label>
        <input class="form-control" value="{{$salarie->prenom}}" name="prenom">
    </div>
    <div class="mb-3">
        <label class="form-label">Adresse</label>
        <input class="form-control" value="{{$salarie->adresse}}" name="adresse">
    </div>
    <div class="mb-3">
        <label class="form-label">Ville</label>
        <input class="form-control" value="{{$salarie->ville}}" name="ville">
    </div>
    <div class="mb-3">
        <label class="form-label">Téléphone</label>
        <input class="form-control" value="{{$salarie->telephone}}" name="telephone">
    </div>
    <div class="mb-3">
        <label class="form-label">Mail</label>
        <input class="form-control" value="{{$salarie->mail}}" name="mail">
    </div>
    <div class="md-3">
        <label for="inputState">Poste</label>
        <select id="inputState" class="form-control" name="poste">
            <option  selected>{{$salarie->poste}}</option>
            <option>CEO</option>
            <option>Chercheur</option>
            <option>Commercial</option>
            <option>Comptable</option>
            <option>DSI</option>
            <option>Informaticien</option>
            <option>Juriste</option>
            <option>Pharmacien</option>
            <option>RH</option>
            <option>Redaction</option>
            <option>Restaurateur</option>
            <option>Secretaire</option>
            <option>Securite</option>
            <option>Technicien de surface</option>
        </select>
    </div>
        <br>
    <div class="mb-3">
        <label class="form-label">Salaire Brut</label>
        <input class="form-control" value="{{$salarie->salaireBrut}}" name="salaireBrut">
    </div>

    <button type="submit" class="btn btn-primary" id="submitUpdate" href="/salaries">Modifier</button>
</form>

<!-- Modal -->

<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModalCenter" id="submitDelete">Supprimer</button>
<br>
<br>

<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Supprimer {{$salarie->prenom}} {{$salarie->nom}} ?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeDelete">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Annuler</button>
                <form method="POST" action="/salarie/delete/{{$salarie->num}}">
                    @method('PUT')
                    @csrf
                    <input type="text" value="{{$salarie->num}}" name="id" hidden>
                    <button type="submit" class="btn btn-danger">Annihilation</button>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
