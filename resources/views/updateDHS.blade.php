@include('importHeader')

<body>

<br>
<div id="updateTitle">
<h2>Modification</h2>
</div>
<br>


<a href="javascript:history.go(-1)"><button type="submit" class="btn btn-secondary" id="retourUpdate">Retour</button></a>
<form method="POST" action="" id="formUpdate">

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @method('PUT')
    @csrf
    <div class="mb-3">
        <label class="form-label">NumSalarie</label>
        <input class="form-control" value={{Auth::user()["id"]}} name="numSalarie" disabled>
    </div>
    <div class="mb-3">
        <label class="form-label">Description</label>
        <input class="form-control" value="{{$dhs->description}}" name="description">
    </div>
    <div class="mb-3">
        <label data-provide="datepicker" class="form-label">Date</label>
        <input placeholder="aaaa-mm-jj" class="form-control" value="{{$dhs->date}}" name="date">
    </div>
    <div class="mb-3">
        <label class="form-label">NbHeure</label>
        <input class="form-control" value="{{$dhs->nbHeure}}" name="nbHeure">
    </div>
    <div class="md-3">
        <label for="inputState">TarifHeure</label>
        <select id="inputState" class="form-control" name="tarifHeure">
            <option  selected>{{$dhs->tarifHeure}}</option>
            <option>1</option>
            <option>2</option>
            <option>3</option>
        </select>
    </div>
        <br>

    <button type="submit" class="btn btn-primary" id="submitUpdate" href="/dhs/update/{{$dhs->id}}">Modifier</button>
</form>

<!-- Modal -->

<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modalDelete" id="submitDelete">Supprimer</button>
<br>
<br>

<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Supprimer la demande numéro : {{$dhs->id}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeDelete">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Annuler</button>
                <form method="POST" action="/dhs/delete/{{$dhs->id}}">
                    @method('DELETE')
                    @csrf
                    <input type="text" value="{{$dhs->id}}" name="id" hidden>
                    <button type="submit" class="btn btn-danger">Annihilation</button>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
