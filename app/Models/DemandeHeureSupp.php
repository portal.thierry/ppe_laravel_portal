<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DemandeHeureSupp extends Model
{
    use HasFactory;

    protected $table = "DEMANDEHEURESUPP";
    public $timestamps = false;
    protected $primaryKey = 'id';

}
