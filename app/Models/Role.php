<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    public  $id;
    public  $libelle;
    public  $droit;

    public function __construct(Array $data = null)
    {

        if($data != null) {

            $this->id = $data["id"];
            $this->libelle = $data["libelle"];
            $this->droit = $data["droit"];

        }
    }


    /**
     * Get the value of id
     */
    public function getId()
    {
    return $this->id;
    }


    /**
     * Set the value of id
     */
    public function setId($id): Role
    {
    $this->id = $id;

    return $this;
    }

    /**
     * Get the value of libelle
     */
    public function getLibelle()
    {
    return $this->libelle;
    }

    /**
     * Set the value of libelle
     */
    public function setLibelle($libelle): Role
    {
    $this->libelle = $libelle;

    return $this;
    }

    /**
     * Get the value of droit
     */
    public function getDroit()
    {
    return $this->droit;
    }

    /**
     * Set the value of droit
     */
    public function setDroit($droit)
    {
    $this->droit = $droit;

    return $this;
    }
}

