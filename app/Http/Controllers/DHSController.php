<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DemandeHeureSupp;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DHSController extends Controller
{

    /**
     * Affiche les informations d'une dhs
     *
     * @param int $id Id du salarie recherche
     * @var DemandeHeureSupp $dhs Recupere les informations sur la dhs recherchee
     * @return view
     * @return redirect Si le user modifie l'URL pour accèder a un autre salarie
     */
    public function show(int $id) {
        $dhs = DemandeHeureSupp::find($id);
        $data = [];
        $data[] = $dhs;
        return view('show1DHS',['data' => $data]);
    }

    /**
     * Affiche les dhs qui commence par une chaine de caracteres
     *
     * @var string $startingLetter Chaine de caractères pour trouver les dhs qui commencent par ...
     * @return view
     */
    public function showFromNav() {
        $id = \Request::get('id');
        $dhs = DemandeHeureSupp::find($id);
        $data = [];
        $data[] = $dhs;
        return view('show1DHS',['data' => $data]);
    }

    /**
     * Affiche les informations de toutes les dhs
     *
     * @return view
     */
    public function showAll(){
        return view('showDHS', ['data' => DemandeHeureSupp::paginate(10)]);
    }

    /**
     * Affiche les informations de toutes les dhs non validees (validee = 0)
     *
     * @return view
     */
    public function showDemandeNonValidee(){
        return view('showDHS', ['data' => DemandeHeureSupp::where('validee', '=', '0')->paginate(10)]);
    }

    /**
     * Affiche le formulaire pour update une dhs
     *
     * @param int $id Id de la dhs recherche
     * @var DemandeHeureSupp $dhs Recupere les informations sur la dhs recherchee
     * @return view
     */
    public function update($id){
        $dhs = DemandeHeureSupp::find($id);
        if(Auth::user()["id"] == $dhs->numSalarie) {
            return view('updateDHS', ['dhs' => $dhs]);
        }
        return redirect()->route('redirect');
    }

    /**
     * Valide une dhs (validee = 1)
     *
     * @param int $id Id du dhs recherche
     * @var DemandeHeureSupp $dhs Recupere les informations sur la dhs recherchee
     * @return view
     */
    public function updateValidation($id){

        $dhs = DemandeHeureSupp::find($id);
        $dhs->validee = '1';
        $dhs->save();

        return redirect('/dhs/'.$id);
    }

    /**
     * Valide une dhs (validee = -1)
     *
     * @param int $id Id du dhs recherche
     * @var DemandeHeureSupp $dhs Recupere les informations sur la dhs recherchee
     * @return view
     */
    public function updateRefus($id){
        $dhs = DemandeHeureSupp::find($id);
        $dhs->validee = '-1';
        $dhs->save();

        return redirect('/dhs/'.$id);
    }

    /**
     * Affiche le formulaire pour add une dhs
     *
     * @var DemandeHeureSupp $dhs Recupere les informations sur la dhs recherchee
     * @return view
     */
    public function add(){
        $data = [];
        $dhs = DemandeHeureSupp::latest('id')->first();
        $data[] = $dhs;
        return view('addDHS', ['dhs' => $dhs]);
    }

    /**
     * Sauvegarde un ajout d'une dhs via le formualire
     *
     * @param Request $request Requete pour enregistrer le salarie
     * @var DemandeHeureSupp $dhs Recupere les informations sur la dhs recherchee
     * @var User $user Creer un user lie aux informations du salarie
     * @return redirect Redirige le user pour voir son ajout a ete effectue
     */
    public function saveFromAdd(Request $request){

        $dhs = new DemandeHeureSupp();

        $dhs->id = DemandeHeureSupp::latest('id')->first()->id+1;
        $dhs->numSalarie = Auth::user()['id'];
        $dhs->description = Request("description");
        $dhs->date = Request("date");
        $dhs->nbHeure = Request("nbHeure");
        $dhs->validee = "0";
        $dhs->tarifHeure = Request("tarifHeure");

         $request->validate([
            'description' => 'required|max:1000',
            'date' => 'required|date',
            'nbHeure' => 'required|numeric',
            'tarifHeure' => 'required|numeric',
         ]);

        $dhs->save();

        if ($request) {
            return redirect('/dhs/'.DemandeHeureSupp::latest('id')->first()->id);
        }
    }

    /**
     * Sauvegarde une modification d'une dhs via le formualire
     *
     * @param Request $request Requete pour enregistrer le salarie
     * @var DemandeHeureSupp $dhs Recupere les informations sur la dhs recherchee
     * @var User $user Creer un user lie aux informations du salarie
     * @return redirect Redirige le user pour voir son ajout a ete effectue
     */
    public function saveFromUpdate($id, Request $request){

        $dhs = DemandeHeureSupp::find($id);
        $dhs->numSalarie = Auth::user()['id'];
        $dhs->description = Request("description");
        $dhs->date = Request("date");
        $dhs->nbHeure = Request("nbHeure");
        $dhs->validee = "0";
        $dhs->tarifHeure = Request("tarifHeure");

         $request->validate([
            'description' => 'required|max:1000',
            'date' => 'required|date',
            'nbHeure' => 'required|numeric',
            'tarifHeure' => 'required|numeric',
         ]);

        $dhs->save();

        if ($request) {
            return redirect('/dhs/'.$dhs->id);
        }
    }

    /**
     * Supprime une dhs
     *
     * @param int $id Id de la dhs recherche
     * @var DemandeHeureSupp $dhs Recupere les informations sur la dhs recherchee
     * @return redirect Redirige le user pour voir la modification effectue
     */
    public function confirmDelete($id, Request $request){
        $dhs = DemandeHeureSupp::find($id);
        $dhs->delete();

        if ($request) {
            return redirect('/dhs');
        }
    }

    /**
     * Affiche toutes les dhs d'un salarie
     *
     * @param int $id Id du salarie dont on cherche les dhs associees
     * @return redirect Redirige le user pour voir la modification effectue
     * @return view Redirige le user pour voir son ajout a ete effectue
     */
    public function showDemande1Salarie($id){
        if(Auth::user()["id"] == $id) {
            return view('showDHS', ['data' => DemandeHeureSupp::where('numSalarie', '=', $id)->paginate(10)]);
        }
        return redirect()->route('redirect');
    }

}
