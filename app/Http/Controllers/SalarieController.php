<?php

namespace App\Http\Controllers;

use Database\Seeders\CreateAdminUserSeeder;
use Database\Seeders\CreateUserUserSeeder;
use Database\Seeders\CreateRHUserSeeder;
use Illuminate\Http\Request;
use App\Models\Salarie;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class SalarieController extends Controller

{

    /**
     * Redirige pour mettre l'id du salarié dans l'URL
     *
     * @var int $user Recupere l'id de l'utilisateur
     * @return view
     */
    public function redirectDashBoard() {
        $user = Auth::user();
        return view('redirectDashBoard',['user' => $user]);
    }

    /**
     * Affiche le dashboard du salarie
     *
     * @var int $user Recupere l'id de l'utilisateur
     * @return view
     * @return redirect Si le user modifie l'URL pour accèder a un autre salarie
     */
    public function showDashBoard(int $id) {
        if(Auth::user()["id"] == $id) {
            $salarie = Salarie::find($id);
            return view('dashBoardSalarie',['salarie' => $salarie]);
        }
        return redirect()->route('redirect');

    }

    /**
     * Affiche les informations d'un salarie
     *
     * @param int $id Id du salarie recherche
     * @var Salarie $salarie Recupere les informations d'un utilisateur
     * @return view
     * @return redirect Si le user modifie l'URL pour accèder a un autre salarie
     */
    public function show(int $id) {
        if(Auth::user()["id"] == $id) {
            $salarie = Salarie::find($id);
            $data = [];
            $data[] = $salarie;
            return view('show1Salarie',['data' => $data]);
        }
        return redirect()->route('redirect');
    }

    /**
     * Affiche les informations de tous les salaries
     *
     * @return view
     */
    public function showAll(){
        return view('showSalaries', ['data' => Salarie::paginate(10)]);
    }

    /**
     * Affiche les salaries qui commence par une chaine de caracteres
     *
     * @var string $startingLetter Chaine de caractères pour trouver les salaries qui commencent par ...
     * @return view
     */
    public function findByName(){

        $startingLetter = \Request::get('nom');
        return view('showSalaries', ['data' => Salarie::where('nom', 'like', $startingLetter.'%')->paginate(10)->appends(['nom' => $startingLetter])]);
    }

    /**
     * Affiche le formulaire pour update un salarie
     *
     * @param int $id Id du salarie recherche
     * @var Salarie $salarie Recupere les informations d'un utilisateur
     * @return view
     */
    public function update($id){
        $salarie = Salarie::find($id);
        return view('updateSalarie', ['salarie' => $salarie]);
    }

    /**
     * Affiche le formulaire pour add un salarie
     *
     * @var Salarie $salarie Recupere les informations d'un utilisateur
     * @return view
     */
    public function add(){
        $salarie = Salarie::latest('num')->first();
        return view('addSalarie', ['salarie' => $salarie]);
    }

    /**
     * Sauvegarde un ajout de salarie via le formualire
     *
     * @param Request $request Requete pour enregistrer le salarie
     * @var Salarie $salarie Recupere les informations d'un utilisateur
     * @var User $user Creer un user lie aux informations du salarie
     * @return redirect Redirige le user pour voir son ajout a ete effectue
     */
    public function saveFromAdd(Request $request){

        $salarie = new Salarie();
        $lastSalarie = Salarie::latest('num')->first();
        $salarie->num = $lastSalarie->num+1;
        $salarie->nom = Request("nom");
        $salarie->prenom = Request("prenom");
        $salarie->adresse = Request("adresse");
        $salarie->ville = Request("ville");
        $salarie->telephone = Request("telephone");
        $salarie->mail = Request("mail");
        $salarie->poste = Request("poste");
        $salarie->salaireBrut = Request("salaireBrut");
        $salarie->suppression= '0';

        $request->validate([
            'nom' => 'required|max:25',
            'prenom' => 'required|max:50',
            'adresse' => 'required|max:50',
            'ville' => 'required|max:50',
            'telephone' => 'required|numeric',
            'mail' => 'required|max:50|email',
            'poste' => 'required|max:30',
            'salaireBrut' => 'required|numeric',
        ]);

        $salarie->save();
        if ($salarie->poste == 'Admin') {
            $user = new CreateAdminUserSeeder();
        }
        elseif ($salarie->poste == 'RH') {
            $user = new CreateRHUserSeeder();
        }
        else{
            $user = new CreateUserUserSeeder();
        }
        $user->setName($salarie->nom);
        $user->setEmail(strtolower($salarie->nom).'.'.strtolower($salarie->prenom).'@gsb.com');

//        $user->runUser($salarie->nom, $salarie->nom.'.'.$salarie->prenom.'@gsb.com');
        $user->run();


        if ($request) {
            return redirect('/salarie/'.$lastSalarie->num+1);
        }
    }

    /**
     * Sauvegarde une modification de salarie via le formualire
     *
     * @param Request $request Requete pour update le salarie
     * @var Salarie $salarie Recupere les informations d'un utilisateur
     * @var User $user Creer un user lie aux informations du salarie
     * @return redirect Redirige le user pour voir la modification effectue
     */
    public function saveFromUpdate($id, Request $request){

        $salarie = Salarie::find($id);

        $salarie->nom = Request("nom");
        $salarie->prenom = Request("prenom");
        $salarie->adresse = Request("adresse");
        $salarie->ville = Request("ville");
        $salarie->telephone = Request("telephone");
        $salarie->mail = Request("mail");
        $salarie->poste = Request("poste");
        $salarie->salaireBrut = Request("salaireBrut");

        $request->validate([
            'nom' => 'required|max:50',
            'prenom' => 'required|max:50',
            'adresse' => 'required|max:50',
            'ville' => 'required|max:50',
            'telephone' => 'required|numeric',
            'mail' => 'required|max:50|email',
            'poste' => 'required|max:30',
            'salaireBrut' => 'required|numeric',
        ]);

        $salarie->save();

        if ($request) {
            return redirect('/salarie/'.$salarie->num);
        }
    }

    /**
     * Update l'attribut suppression a 1 (la confirmation doit etre validee par l'admin)
     *
     * @param int $id Id du salarie recherche
     * @var Salarie $salarie Recupere les informations d'un utilisateur
     * @return redirect Redirige le user pour voir la modification effectue
     */
    public function confirmDelete($id){
        $salarie = Salarie::find($id);
        $salarie->suppression = '1';
        $salarie->save();
        return redirect('/salarie/'.$salarie->num);
    }

    /**
     * Update l'attribut suppression a 1 (la confirmation doit etre validee par l'admin)
     *
     * @param int $id Id du salarie recherche
     * @param Request $request Requete pour update le salarie
     * @var Salarie $salarie Recupere les informations d'un utilisateur
     * @return redirect Redirige le user pour voir la modification effectue
     */
    public function confirmDeleteAdmin($id, Request $request){
        $salarie = Salarie::find($id);
        $user = User::find($id);
        $salarie->delete();
        $user->delete();

        if ($request) {
            return redirect('/salaries/supp');
        }
    }

    /**
     * Affiche les informations de tous les salaries dont l'attribut suppression est a 1 (il manque l'accord de l'admin pour les supprimerl)
     *
     * @return view
     */
    public function showSalarieSupp(){
        return view('showSalariesSupp', ['data' => Salarie::where('suppression', '=', '1')->paginate(10)]);
    }

}
