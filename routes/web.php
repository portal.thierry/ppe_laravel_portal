<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('redirect');
});

//dashboard
Route::get('/espace/{id}',[\App\Http\Controllers\SalarieController::class,"showDashBoard"])
->where("id" , "[0-9]+")->middleware('auth');

Route::get('/redirect',[\App\Http\Controllers\SalarieController::class,"redirectDashBoard"])->name('redirect')->middleware('auth');



//Route Salarie
Route::get('/salarie/{id}',[\App\Http\Controllers\SalarieController::class,"show"])
->where("id" , "[0-9]+")->middleware('auth');

Route::get('/salaries',[\App\Http\Controllers\SalarieController::class,"showAll"])->middleware('auth')->middleware('checkrolerh');

Route::get('/salaries/findByNom', [\App\Http\Controllers\SalarieController::class,"findByName"])->middleware('auth')->middleware('checkrolerh');

Route::get("/salarie/add/", [\App\Http\Controllers\SalarieController::class, "add"])->middleware('auth')->middleware('checkrolerh');

Route::put("/salarie/add/", [\App\Http\Controllers\SalarieController::class, "saveFromAdd"])->middleware('auth')->middleware('checkrolerh');

Route::get("/salarie/update/{id}", [\App\Http\Controllers\SalarieController::class, "update"])
->where("id", "[0-9]+")->middleware('auth')->middleware('checkrolerh');

Route::put("/salarie/update/{id}", [\App\Http\Controllers\SalarieController::class, "saveFromUpdate"])
->where("id", "[0-9]+")->middleware('auth')->middleware('checkrolerh');

Route::put("/salarie/delete/{id}", [\App\Http\Controllers\SalarieController::class, "confirmDelete"])
->where("id", "[0-9]+")->middleware('auth')->middleware('checkrolerh');

Route::get('/salaries/supp',[\App\Http\Controllers\SalarieController::class,"showSalarieSupp"])->middleware('auth')->middleware('checkroleadmin');

Route::delete("/salarie/supp/delete/{id}", [\App\Http\Controllers\SalarieController::class, "confirmDeleteAdmin"])
->where("id", "[0-9]+")->middleware('auth')->middleware('checkroleadmin');


//Route DemandeHeureSupp
Route::get('/dhs/{id}',[\App\Http\Controllers\DHSController::class,"show"])
->where("id" , "[0-9]+")->middleware('auth')->middleware('checkrolerh');

Route::get('/dhs/findFromNav',[\App\Http\Controllers\DHSController::class,"showFromNav"])->middleware('auth')->middleware('checkrolerh');

Route::get('/dhs',[\App\Http\Controllers\DHSController::class,"showAll"])->middleware('auth')->middleware('checkrolerh');

Route::get('/dhs/nonValidee',[\App\Http\Controllers\DHSController::class,"showDemandeNonValidee"])->middleware('auth')->middleware('checkrolerh');

Route::get("/dhs/add/", [\App\Http\Controllers\DHSController::class, "add"])->middleware('auth')->middleware('checkroleuser');

Route::put("/dhs/add/", [\App\Http\Controllers\DHSController::class, "saveFromAdd"])->middleware('auth')->middleware('checkroleuser');

Route::get("/dhs/update/{id}", [\App\Http\Controllers\DHSController::class, "update"])
->where("id", "[0-9]+")->middleware('auth')->middleware('checkroleuser');

Route::get("/dhs/updateValidation/{id}", [\App\Http\Controllers\DHSController::class, "updateValidation"])
->where("id", "[0-9]+")->middleware('auth')->middleware('checkrolerh');

Route::get("/dhs/updateRefus/{id}", [\App\Http\Controllers\DHSController::class, "updateRefus"])
->where("id", "[0-9]+")->middleware('auth')->middleware('checkrolerh');

Route::put("/dhs/update/{id}", [\App\Http\Controllers\DHSController::class, "saveFromUpdate"])
->where("id", "[0-9]+")->middleware('auth')->middleware('checkroleuser');

Route::delete("/dhs/delete/{id}", [\App\Http\Controllers\DHSController::class, "confirmDelete"])
->where("id", "[0-9]+")->middleware('auth')->middleware('checkroleuser');

Route::get("/dhs/salarie/{id}", [\App\Http\Controllers\DHSController::class, "showDemande1Salarie"])
->where("id", "[0-9]+")->middleware('auth')->middleware('checkroleuser');


Route::get('/home', [HomeController::class, 'index'])->name('home');


Route::group(['middleware' => ['auth']], function() {
    Route::resource('users', UserController::class)->middleware('checkroleadmin');
});

Auth::routes();
